﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurniDefender
{
    public class FurniTools
    {
        public static bool isByteArrayAsset(string assetNameFull)
        {
            return (assetNameFull.EndsWith("_manifest") || assetNameFull.EndsWith("_index") || assetNameFull.EndsWith("_visualization") || assetNameFull.EndsWith("_assets") || assetNameFull.EndsWith("_logic"));
        }

        public static string buildPrivateNamespace(string furniName)
        {
            StringBuilder nsb = new StringBuilder();
            nsb.AppendLine("; QName(PackageNamespace(\"\"), \"" + furniName + "\") -> iinit");
            nsb.AppendLine("#privatens   50 \"" + furniName + "/iinit#0\"");
            nsb.AppendLine("; QName(PackageNamespace(\"\"), \"" + furniName + "\") -> iinit");
            nsb.AppendLine("#privatens   50 \"" + furniName + "/iinit#1\"");
            nsb.AppendLine("; QName(PackageNamespace(\"mx.utils\"), \"NameUtil\")");
            nsb.AppendLine("#privatens   13 \"mx.utils:NameUtil\"");

            return nsb.ToString();
        }


        public static string buildMainAsasm(string furniName, Dictionary<int, string> assets)
        {
            StringBuilder asm = new StringBuilder();
            asm.AppendLine("#include \"" + furniName + "-0.privatens.asasm\"");
            asm.AppendLine("program");
            asm.AppendLine("minorversion 16");
            asm.AppendLine("majorversion 46");
            asm.AppendLine("#include \"" + furniName + ".script.asasm\"");
            asm.AppendLine("#include \"mxcore/IFlexAsset.script.asasm\"");
            asm.AppendLine("#include \"mxcore/FlexBitmap.script.asasm\"");
            asm.AppendLine("#include \"mxcore/IFlexDisplayObject.script.asasm\"");
            asm.AppendLine("#include \"mxcore/BitmapAsset.script.asasm\"");
            asm.AppendLine("#include \"mxcore/ByteArrayAsset.script.asasm\"");
            asm.AppendLine("#include \"mxcore/mx_internal.script.asasm\"");
            asm.AppendLine("#include \"mxutils/NameUtil.script.asasm\"");
            asm.AppendLine("#include \"mxcore/IRepeaterClient.script.asasm\"");

            foreach (KeyValuePair<int, string> asset in assets)
            {
                int assetSlot = asset.Key;
                string assetName = asset.Value;
                string assetNameFull = furniName + "_" + assetName;

                asm.AppendLine("#include \"" + assetNameFull + ".script.asasm\"");
            }

            asm.AppendLine("end ; program");

            return asm.ToString();
        }

        public static string buildFurniClass(string furniName, Dictionary<int,string> assets, bool authorInfo = false, string authorName = "", string ignoredDomains = "")
        {
            StringBuilder mcl = new StringBuilder();
            mcl.AppendLine("class");
            mcl.AppendLine("refid \"" + furniName + "\"");
            mcl.AppendLine("instance QName(PackageNamespace(\"\"), \"" + furniName + "\")");
            mcl.AppendLine("extends QName(PackageNamespace(\"flash.display\"), \"Sprite\")");
            mcl.AppendLine("flag SEALED");
            mcl.AppendLine("flag PROTECTEDNS");
            mcl.AppendLine("protectedns ProtectedNamespace(\":" + furniName + "\")");
            mcl.AppendLine("iinit");
            mcl.AppendLine("name \":" + furniName + "/" + furniName + "\"");
            mcl.AppendLine("refid \"" + furniName + "/iinit\"");
            mcl.AppendLine("body");
            mcl.AppendLine("maxstack 2");
            mcl.AppendLine("localcount 2");
            mcl.AppendLine("initscopedepth 10");
            mcl.AppendLine("maxscopedepth 11");
            mcl.AppendLine("code");
            mcl.AppendLine("getlocal0");
            mcl.AppendLine("pushscope");

            if (authorInfo)
            {
                mcl.AppendLine("debug               1, \"hideInfo\", 0, 48");
                mcl.AppendLine("getlex              QName(PackageNamespace(\"flash.external\"), \"ExternalInterface\")");
                mcl.AppendLine("pushstring          \"var ignoreDomains = '" + ignoredDomains + "'; for (var i = 0; i < domainArrayString.split(';').length; i++) { if(window.location.href.includes(domainArrayString.split(';')[i])) { return true; } } return false;\"");
                mcl.AppendLine("callproperty        Multiname(\"call\", [PackageNamespace(\"\"), Namespace(\"http://adobe.com/AS3/2006/builtin\"), PackageInternalNs(\"\"), StaticProtectedNs(\"flash.display:Sprite\"), StaticProtectedNs(\"flash.display:DisplayObjectContainer\"), StaticProtectedNs(\"flash.display:InteractiveObject\"), StaticProtectedNs(\"flash.display:DisplayObject\"), StaticProtectedNs(\"flash.events:EventDispatcher\"), PrivateNamespace(\":" + furniName + "\", \"" + furniName + "/iinit#1\"), ProtectedNamespace(\":" + furniName + "\"), StaticProtectedNs(\":" + furniName + "\"), PrivateNamespace(\"" + furniName + ".as$0\", \"" + furniName + "/iinit#0\")]), 1");
                mcl.AppendLine("convert_b");
                mcl.AppendLine("setlocal1");
                mcl.AppendLine("getlocal1");
                mcl.AppendLine("not");
                mcl.AppendLine("iffalse             L14");

                mcl.AppendLine("getlex              QName(PackageNamespace(\"flash.external\"), \"ExternalInterface\")");
                mcl.AppendLine("pushstring          \"function() { console.log(\'%cThe furni " + furniName + " was made by me! You can use this furni if you are a nice guy and mention me somewhere c: \n\nKind regards, " + authorName + "\', \'background: #14b7cb; color: #ffffff; font-size: x-large\'); }\"");
                mcl.AppendLine("callpropvoid        Multiname(\"call\", [PackageNamespace(\"\"), Namespace(\"http://adobe.com/AS3/2006/builtin\"), PackageInternalNs(\"\"), StaticProtectedNs(\"flash.display:Sprite\"), StaticProtectedNs(\"flash.display:DisplayObjectContainer\"), StaticProtectedNs(\"flash.display:InteractiveObject\"), StaticProtectedNs(\"flash.display:DisplayObject\"), StaticProtectedNs(\"flash.events:EventDispatcher\"), PrivateNamespace(\":" + furniName + "\", \"" + furniName + "/iinit#1\"), ProtectedNamespace(\":" + furniName + "\"), StaticProtectedNs(\":" + furniName + "\"), PrivateNamespace(\"" + furniName + ".as$0\", \"" + furniName + "/iinit#0\")]), 1");

                mcl.AppendLine("L14:");
            }

            mcl.AppendLine("getlocal0");
            mcl.AppendLine("constructsuper      0");
            mcl.AppendLine("returnvoid");
            mcl.AppendLine("end ; code");
            mcl.AppendLine("end ; body");
            mcl.AppendLine("end ; method");
            mcl.AppendLine("end ; instance");
            mcl.AppendLine("cinit");
            mcl.AppendLine("name \"\"");
            mcl.AppendLine("refid \"" + furniName + "/cinit\"");
            mcl.AppendLine("body");
            mcl.AppendLine("maxstack 2");
            mcl.AppendLine("localcount 1");
            mcl.AppendLine("initscopedepth 10");
            mcl.AppendLine("maxscopedepth 11");
            mcl.AppendLine("code");
            mcl.AppendLine("getlocal0");
            mcl.AppendLine("pushscope");

            //assets
            foreach (KeyValuePair<int, string> asset in assets)
            {
                int assetSlot = asset.Key;
                string assetName = asset.Value;
                string assetNameFull = furniName + "_" + assetName;

                mcl.AppendLine("");
                mcl.AppendLine("findproperty        QName(PackageNamespace(\"\"), \"" + assetName + "\")");
                mcl.AppendLine("getlex              QName(PackageNamespace(\"\"), \"" + assetNameFull + "\")");
                mcl.AppendLine("initproperty        QName(PackageNamespace(\"\"), \"" + assetName + "\")");
                mcl.AppendLine("");
            }

            mcl.AppendLine("returnvoid");
            mcl.AppendLine("end ; code");
            mcl.AppendLine("end ; body");
            mcl.AppendLine("end ; method");

            foreach (KeyValuePair<int, string> asset in assets)
            {
                int assetSlot = asset.Key;
                string assetName = asset.Value;
                string assetNameFull = furniName + "_" + assetName;

                mcl.AppendLine("trait "+ (isByteArrayAsset(assetNameFull) ? "const" : "slot") + " QName(PackageNamespace(\"\"), \"" + assetName + "\") slotid " + assetSlot + " type QName(PackageNamespace(\"\"), \"Class\") end");
            }

            mcl.AppendLine("end ; class");

            return mcl.ToString();
        }

        public static string buildFurniScript(string furniName)
        {
            StringBuilder msc = new StringBuilder();
            msc.AppendLine("script ; 0");
            msc.AppendLine("sinit");
            msc.AppendLine("name \"\"");
            msc.AppendLine("refid \"" + furniName + ".sinit\"");
            msc.AppendLine("body");
            msc.AppendLine("maxstack 2");
            msc.AppendLine("localcount 1");
            msc.AppendLine("initscopedepth 1");
            msc.AppendLine("maxscopedepth 8");
            msc.AppendLine("code");
            msc.AppendLine("getlocal0");
            msc.AppendLine("pushscope");

            msc.AppendLine("getscopeobject      0");
            msc.AppendLine("getlex              QName(PackageNamespace(\"\"), \"Object\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.events\"), \"EventDispatcher\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"DisplayObject\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"InteractiveObject\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"DisplayObjectContainer\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"Sprite\")");
            msc.AppendLine("pushscope");

            msc.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"Sprite\")");
            msc.AppendLine("newclass            \"" + furniName + "\"");
            msc.AppendLine("popscope");
            msc.AppendLine("popscope");
            msc.AppendLine("popscope");
            msc.AppendLine("popscope");
            msc.AppendLine("popscope");
            msc.AppendLine("popscope");

            msc.AppendLine("initproperty        QName(PackageNamespace(\"\"), \"" + furniName + "\")");
            msc.AppendLine("returnvoid");
            msc.AppendLine("end ; code");
            msc.AppendLine("end ; body");
            msc.AppendLine("end ; method");

            msc.AppendLine("trait class QName(PackageNamespace(\"\"), \"" + furniName + "\") slotid 1");
            msc.AppendLine("#include \"" + furniName + ".class.asasm\"");
            msc.AppendLine("end ; trait");
            msc.AppendLine("end ; script");

            return msc.ToString();
        }

        public static string buildAssetClass(string assetNameFull)
        {
            StringBuilder assetClass = new StringBuilder();
            assetClass.AppendLine("class");
            assetClass.AppendLine("refid \"" + assetNameFull + "\"");
            assetClass.AppendLine("instance QName(PackageNamespace(\"\"), \"" + assetNameFull + "\")");
            assetClass.AppendLine("extends QName(PackageNamespace(\"mx.core\"), \"" + (isByteArrayAsset(assetNameFull) ? "ByteArrayAsset" : "BitmapAsset") + "\")");
            assetClass.AppendLine("flag SEALED");
            assetClass.AppendLine("flag PROTECTEDNS");
            assetClass.AppendLine("protectedns ProtectedNamespace(\"" + assetNameFull + "\")");
            assetClass.AppendLine("iinit");
            assetClass.AppendLine("refid \"" + assetNameFull + "/iinit\"");
            assetClass.AppendLine("body");
            assetClass.AppendLine("maxstack 1");
            assetClass.AppendLine("localcount 1");
            assetClass.AppendLine("initscopedepth " + (isByteArrayAsset(assetNameFull) ? "6" : "9"));
            assetClass.AppendLine("maxscopedepth " + (isByteArrayAsset(assetNameFull) ? "7" : "10"));
            assetClass.AppendLine("code");
            assetClass.AppendLine("getlocal0");
            assetClass.AppendLine("constructsuper      0");
            assetClass.AppendLine("returnvoid");
            assetClass.AppendLine("end ; code");
            assetClass.AppendLine("end ; body");
            assetClass.AppendLine("end ; method");
            assetClass.AppendLine("end ; instance");
            assetClass.AppendLine("cinit");
            assetClass.AppendLine("refid \"" + assetNameFull + "/cinit\"");
            assetClass.AppendLine("body");
            assetClass.AppendLine("maxstack 1");
            assetClass.AppendLine("localcount 1");
            assetClass.AppendLine("initscopedepth " + (isByteArrayAsset(assetNameFull) ? "5" : "8"));
            assetClass.AppendLine("maxscopedepth " + (isByteArrayAsset(assetNameFull) ? "6" : "9"));
            assetClass.AppendLine("code");
            assetClass.AppendLine("getlocal0");
            assetClass.AppendLine("pushscope");
            assetClass.AppendLine("returnvoid");
            assetClass.AppendLine("end ; code");
            assetClass.AppendLine("end ; body");
            assetClass.AppendLine("end ; method");
            assetClass.AppendLine("end ; class");

            return assetClass.ToString();
        }

        public static string buildAssetScript(string assetNameFull, int slot)
        {
            StringBuilder assetScript = new StringBuilder();
            assetScript.AppendLine("script ; " + slot);
            assetScript.AppendLine("sinit");
            assetScript.AppendLine("refid \"" + assetNameFull + ".sinit\"");
            assetScript.AppendLine("body");
            assetScript.AppendLine("maxstack 2");
            assetScript.AppendLine("localcount 1");
            assetScript.AppendLine("initscopedepth 1");

            assetScript.AppendLine("maxscopedepth " + (isByteArrayAsset(assetNameFull) ? "5" : "8"));

            assetScript.AppendLine("code");
            assetScript.AppendLine("getlocal0");
            assetScript.AppendLine("pushscope");

            assetScript.AppendLine("findpropstrict      Multiname(\"" + assetNameFull + "\", [PackageNamespace(\"\")])");
            assetScript.AppendLine("getlex              QName(PackageNamespace(\"\"), \"Object\")");
            assetScript.AppendLine("pushscope");

            if (isByteArrayAsset(assetNameFull))
            {
                assetScript.AppendLine("getlex              QName(PackageNamespace(\"flash.utils\"), \"ByteArray\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"mx.core\"), \"ByteArrayAsset\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"mx.core\"), \"ByteArrayAsset\")");
                assetScript.AppendLine("newclass            \"" + assetNameFull + "\"");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
            }
            else
            {
                assetScript.AppendLine("getlex              QName(PackageNamespace(\"flash.events\"), \"EventDispatcher\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"DisplayObject\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"flash.display\"), \"Bitmap\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"mx.core\"), \"FlexBitmap\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"mx.core\"), \"BitmapAsset\")");
                assetScript.AppendLine("pushscope");

                assetScript.AppendLine("getlex              QName(PackageNamespace(\"mx.core\"), \"BitmapAsset\")");
                assetScript.AppendLine("newclass            \"" + assetNameFull + "\"");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
                assetScript.AppendLine("popscope");
            }

            assetScript.AppendLine("initproperty        QName(PackageNamespace(\"\"), \"" + assetNameFull + "\")");
            assetScript.AppendLine("returnvoid");
            assetScript.AppendLine("end ; code");
            assetScript.AppendLine("end ; body");
            assetScript.AppendLine("end ; method");

            assetScript.AppendLine("trait class QName(PackageNamespace(\"\"), \"" + assetNameFull + "\")");
            assetScript.AppendLine("#include \"" + assetNameFull + ".class.asasm\"");
            assetScript.AppendLine("end ; trait");
            assetScript.AppendLine("end ; script");

            return assetScript.ToString();
        }

        public static void rabcdasm(string furniName)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Disassemble");

            Process process = new Process();
            process.StartInfo.FileName = "rabcdasm.exe";
            process.StartInfo.Arguments = @".\furniture\" + furniName + "-0.abc";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            process.WaitForExit();
        }

        public static void rabcasm(string furniName)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Assemble");

            Process process = new Process();
            process.StartInfo.FileName = "rabcasm.exe";
            process.StartInfo.Arguments = @".\furniture\" + furniName + @"-0\" + furniName + "-0.main.asasm";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            process.WaitForExit();
        }

        public static void abcexport(string furniName)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Export ABC");

            Process process = new Process();
            process.StartInfo.FileName = "abcexport.exe";
            process.StartInfo.Arguments = @".\furniture\" + furniName + @".swf";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            process.WaitForExit();
        }

        public static void abcreplace(string furniName)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Replace ABC");

            Process process = new Process();
            process.StartInfo.FileName = "abcreplace.exe";
            process.StartInfo.Arguments = @".\furniture\" + furniName + @".swf 0 " + @".\furniture\" + furniName + @"-0\" + furniName + @"-0.main.abc";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            process.WaitForExit();
        }

        public static Dictionary<int, string> getAssets(string furniName, bool keepVisualizationData)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Export bin files and get assets");

            Dictionary<int, string> assets = new Dictionary<int, string>();

            Process process = new Process();
            process.StartInfo.FileName = "swfbinexport.exe";
            process.StartInfo.Arguments = @".\furniture\" + furniName + @".swf";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            Console.Write(process.StandardOutput.ReadToEnd());
            process.WaitForExit();

            DirectoryInfo d = new DirectoryInfo(@".\furniture\");
            bool flag = false;
            foreach (var file in d.GetFiles("*.bin"))
            {
                bool isVisualizationData = false;
                if (file.Name.StartsWith(furniName + "-"))
                {
                    using (StreamReader sr = new StreamReader(file.FullName))
                    {
                        string line = null;
                        int slot = 6;
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Contains("asset") && line.Contains("mimeType"))
                            {
                                string assetName = line;
                                assetName = assetName.Split(new string[] { "name=\"" }, StringSplitOptions.None)[1];
                                assetName = assetName.Split('"')[0];

                                if (!(assetName.EndsWith("manifest") || assetName.EndsWith("index") || assetName.EndsWith("visualization") || assetName.EndsWith("assets") || assetName.EndsWith("logic")))
                                {
                                    assets.Add(slot, assetName);
                                    slot++;
                                }
                            }
                            if (line.Contains("<manifest>"))
                            {
                                flag = true;
                            }

                            if(line.Contains("<visualizationData") && keepVisualizationData)
                            {
                                isVisualizationData = true;
                            }
                        }
                    }
                    if(!isVisualizationData)
                    {
                        File.Delete(file.FullName);
                    }
                }
            }
            if (flag)
            {
                assets.Add(5, furniName + "_logic");
                assets.Add(4, furniName + "_assets");
                assets.Add(3, furniName + "_visualization");
                assets.Add(2, "index");
                assets.Add(1, "manifest");

                return assets;
            }
            return null;
        }



        public static void removeGraphicsTag(string furniName)
        {
            Console.WriteLine("[" + furniName.ToUpper() + "] Remove graphics tag");

            DirectoryInfo d = new DirectoryInfo(@".\furniture\");
            foreach (var file in d.GetFiles("*.bin"))
            {
                if(!file.Name.Contains(furniName))
                {
                    continue;
                }

                string binContent = "";
                string binID = file.Name.Split('-')[file.Name.Split('-').Length-1].Replace(".bin","");


                using (StreamReader sr = new StreamReader(file.FullName))
                {
                    binContent = sr.ReadToEnd();
                }
                if (binContent.Contains("<graphics>"))
                {
                    binContent = binContent.Replace("<graphics>", "").Replace("</graphics>", "");

                    // Delete old file
                    if (File.Exists(file.FullName))
                    {
                        File.Delete(file.FullName);
                    }

                    // Write new file
                    using (StreamWriter sw = new StreamWriter(file.FullName))
                    {
                        sw.Write(binContent);
                    }

                    Console.WriteLine(binContent);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(@".\furniture\" + furniName + @".swf " + binID + " " + @".\furniture\" + furniName + @"-0\" + file.Name);
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Process process = new Process();
                    process.StartInfo.FileName = "swfbinreplace.exe";
                    process.StartInfo.Arguments = @".\furniture\" + furniName + @".swf " + binID + " " +  @".\furniture\" + furniName + @"-0\" + file.Name;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;
                    process.Start();
                    Console.Write(process.StandardOutput.ReadToEnd());
                    process.WaitForExit();

                    if (File.Exists(file.FullName))
                    {
                        File.Delete(file.FullName);
                    }

                    return;
                }
            }
        }
    }
}
