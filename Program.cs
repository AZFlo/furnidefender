﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurniDefender
{
    class Program
    {
        // Copyright notice
        static bool author_info = false;
        static string author = "FurniDefender";
        static string ignoredDomains = "furnidefender.com;furnidefender.de;furnidefender.fr;";
           
        // Misc
        static bool close_if_done = false;
        static bool removeGraphicsTag = false; // Experimental - in development


        static void Main(string[] args)
        {
            #region args
            List<string> arguments = new List<string>(args);
            foreach (string arg in arguments)
            {
                if (arg.Contains("/author="))
                {
                    author_info = true;
                    author = arg.Replace("/author=", "");
                }

                if (arg.Contains("/ignoreddomains="))
                {
                    ignoredDomains = arg.Replace("/ignoreddomains=", "");
                }

                if (arg.Contains("/closeifdone"))
                {
                    close_if_done = true;
                }
            }
            #endregion

            Console.Title = "FurniDefender v1.0.3 :: " + (author_info ? "Remove protection and insert author copyright notice" : "Remove protection only");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("FurniDefender v1.0.3");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Mode: " + (author_info ? "Remove protection and insert author copyright notice" : "Remove protection only"));
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine();
            Console.WriteLine();


            // Create missing directory
            if (!Directory.Exists("finished"))
            {
                Directory.CreateDirectory("finished");
            }

            DirectoryInfo d = new DirectoryInfo("furniture");

            foreach (var file in d.GetFiles("*.swf"))
            {
                string furniName = file.Name.ToLower().Replace(".swf","");

                #region clean
                if (Directory.Exists(@".\furniture\" + furniName + "-0"))
                {
                    string[] files = System.IO.Directory.GetFiles(@".\furniture\", "*.abc", System.IO.SearchOption.TopDirectoryOnly);
                    foreach (string f in files)
                    {
                        File.Delete(f);
                    }

                    Directory.Delete(@".\furniture\" + furniName + "-0", true);
                }

                #endregion

                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("[" + furniName.ToUpper() + "] Processing...");
                Console.ForegroundColor = ConsoleColor.Gray;

                try
                {


                    FurniTools.abcexport(furniName);
                    FurniTools.rabcdasm(furniName);

                    var assets = FurniTools.getAssets(furniName, removeGraphicsTag);
                    if (assets != null)
                    {
                        #region Delete known defender files
                        string[] files = System.IO.Directory.GetFiles(@".\furniture\" + furniName + "-0", "%*.asasm", System.IO.SearchOption.TopDirectoryOnly);
                        foreach (string f in files)
                        {
                            File.Delete(f);
                        }
                        #endregion

                        #region Build furniName.class.asasm
                        string furniClassAsasm = FurniTools.buildFurniClass(furniName, assets, author_info, author, ignoredDomains);

                        // Remove old file
                        if (File.Exists(@".\furniture\" + furniName + @"-0\" + furniName + ".class.asasm"))
                        {
                            File.Delete(@".\furniture\" + furniName + @"-0\" + furniName + ".class.asasm");
                        }

                        // Write new file
                        using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + furniName + ".class.asasm"))
                        {
                            sw.Write(furniClassAsasm);
                        }
                        #endregion

                        #region Build furniName.script.asasm
                        string furniScriptAsasm = FurniTools.buildFurniScript(furniName);

                        // Remove old file
                        if (File.Exists(@".\furniture\" + furniName + @"-0\" + furniName + ".script.asasm"))
                        {
                            File.Delete(@".\furniture\" + furniName + @"-0\" + furniName + ".script.asasm");
                        }

                        // Write new file
                        using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + furniName + ".script.asasm"))
                        {
                            sw.Write(furniScriptAsasm);
                        }
                        #endregion

                        #region Build furniName-0.main.asasm
                        string mainAsasmContent = FurniTools.buildMainAsasm(furniName, assets);

                        // Remove old file
                        if (File.Exists(@".\furniture\" + furniName + @"-0\" + furniName + "-0.main.asasm"))
                        {
                            File.Delete(@".\furniture\" + furniName + @"-0\" + furniName + "-0.main.asasm");
                        }

                        // Write new file
                        using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + furniName + "-0.main.asasm"))
                        {
                            sw.Write(mainAsasmContent);
                        }
                        #endregion

                        #region Build furniName-0.privatens.asasm
                        string privateNameSpaceContent = FurniTools.buildPrivateNamespace(furniName);

                        // Remove old file
                        if (File.Exists(@".\furniture\" + furniName + @"-0\" + furniName + "-0.privatens.asasm"))
                        {
                            File.Delete(@".\furniture\" + furniName + @"-0\" + furniName + "-0.privatens.asasm");
                        }

                        // Write new file
                        using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + furniName + "-0.privatens.asasm"))
                        {
                            sw.Write(privateNameSpaceContent);
                        }
                        #endregion

                        #region Build asset files
                        foreach (KeyValuePair<int, string> asset in assets)
                        {
                            int assetSlot = asset.Key;
                            string assetName = asset.Value;
                            string assetNameFull = furniName + "_" + assetName;

                            string assetClass = FurniTools.buildAssetClass(assetNameFull);
                            string assetScript = FurniTools.buildAssetScript(assetNameFull, assetSlot);

                            #region class
                            // Remove old file
                            if (File.Exists(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".class.asasm"))
                            {
                                File.Delete(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".class.asasm");
                            }

                            // Write new file
                            using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".class.asasm"))
                            {
                                sw.Write(assetClass);
                            }
                            #endregion

                            #region script
                            // Remove old file
                            if (File.Exists(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".script.asasm"))
                            {
                                File.Delete(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".script.asasm");
                            }

                            // Write new file
                            using (StreamWriter sw = new StreamWriter(@".\furniture\" + furniName + @"-0\" + assetNameFull + ".script.asasm"))
                            {
                                sw.Write(assetScript);
                            }
                            #endregion

                            Console.WriteLine("[" + furniName.ToUpper() + "] Build " + (FurniTools.isByteArrayAsset(assetNameFull) ? "ByteArrayAsset" : "BitmapAsset") + " Asset: " + assetName);
                        }
                        #endregion

                        #region create mx core and mx tools files
                        if (!Directory.Exists(@".\furniture\" + furniName + @"-0\mxcore\"))
                        {
                            Directory.CreateDirectory(@".\furniture\" + furniName + @"-0\mxcore\");
                        }

                        if (!Directory.Exists(@".\furniture\" + furniName + @"-0\mxutils\"))
                        {
                            Directory.CreateDirectory(@".\furniture\" + furniName + @"-0\mxutils\");
                        }

                        string NameUtilClass = Properties.Resources.NameUtil_class;
                        string NameUtilScript = Properties.Resources.NameUtil_script;

                        rewriteFile(furniName, "mxutils", "NameUtil.class", NameUtilClass);
                        rewriteFile(furniName, "mxutils", "NameUtil.script", NameUtilScript);


                        string BitmapAssetClass = Properties.Resources.BitmapAsset_class;
                        string BitmapAssetScript = Properties.Resources.BitmapAsset_script;
                        string ByteArrayAssetClass = Properties.Resources.ByteArrayAsset_class;
                        string ByteArrayAssetScript = Properties.Resources.ByteArrayAsset_script;
                        string FlexBitmapClass = Properties.Resources.FlexBitmap_class;
                        string FlexBitmapScript = Properties.Resources.FlexBitmap_script;
                        string IFlexAssetClass = Properties.Resources.IFlexAsset_class;
                        string IFlexAssetScript = Properties.Resources.IFlexAsset_script;
                        string IFlexDisplayObjectClass = Properties.Resources.IFlexDisplayObject_class;
                        string IFlexDisplayObjectScript = Properties.Resources.IFlexDisplayObject_script;
                        string IRepeaterClientClass = Properties.Resources.IRepeaterClient_class;
                        string IRepeaterClientScript = Properties.Resources.IRepeaterClient_script;
                        string mx_internal_script = Properties.Resources.mx_internal_script;

                        rewriteFile(furniName, "mxcore", "BitmapAsset.class", BitmapAssetClass);
                        rewriteFile(furniName, "mxcore", "BitmapAsset.script", BitmapAssetScript);

                        rewriteFile(furniName, "mxcore", "ByteArrayAsset.class", ByteArrayAssetClass);
                        rewriteFile(furniName, "mxcore", "ByteArrayAsset.script", ByteArrayAssetScript);

                        rewriteFile(furniName, "mxcore", "FlexBitmap.class", FlexBitmapClass);
                        rewriteFile(furniName, "mxcore", "FlexBitmap.script", FlexBitmapScript);

                        rewriteFile(furniName, "mxcore", "IFlexAsset.class", IFlexAssetClass);
                        rewriteFile(furniName, "mxcore", "IFlexAsset.script", IFlexAssetScript);

                        rewriteFile(furniName, "mxcore", "IFlexDisplayObject.class", IFlexDisplayObjectClass);
                        rewriteFile(furniName, "mxcore", "IFlexDisplayObject.script", IFlexDisplayObjectScript);

                        rewriteFile(furniName, "mxcore", "IRepeaterClient.class", IRepeaterClientClass);
                        rewriteFile(furniName, "mxcore", "IRepeaterClient.script", IRepeaterClientScript);

                        rewriteFile(furniName, "mxcore", "mx_internal.script", mx_internal_script);
                        #endregion

                        FurniTools.rabcasm(furniName);
                        FurniTools.abcreplace(furniName);

                        // Experimental
                        if (removeGraphicsTag)
                        {
                            FurniTools.removeGraphicsTag(furniName);
                        }

                        #region clean
                        if (Directory.Exists(@".\furniture\" + furniName + "-0"))
                        {
                            string[] cfiles = System.IO.Directory.GetFiles(@".\furniture\", "*.abc", System.IO.SearchOption.TopDirectoryOnly);
                            foreach (string f in cfiles)
                            {
                                File.Delete(f);
                            }

                            Directory.Delete(@".\furniture\" + furniName + "-0", true);
                        }

                        if (File.Exists(@".\finished\" + furniName + ".swf"))
                        {
                            File.Delete(@".\finished\" + furniName + ".swf");
                        }

                        File.Copy(@".\furniture\" + furniName + ".swf", @".\finished\" + furniName + ".swf");

                        File.Delete(@".\furniture\" + furniName + ".swf");
                        #endregion
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("[" + furniName.ToUpper() + "] Error: There are no assets defined in manifest bin-file");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                }
                catch (Exception ex)
                {
                    #region clean
                    if (Directory.Exists(@".\furniture\" + furniName + "-0"))
                    {
                        string[] cfiles = System.IO.Directory.GetFiles(@".\furniture\", "*.abc", System.IO.SearchOption.TopDirectoryOnly);
                        foreach (string f in cfiles)
                        {
                            if(f.Contains(furniName))
                            {
                                File.Delete(f);
                            }
                        }

                        Directory.Delete(@".\furniture\" + furniName + "-0", true);
                    }
                    #endregion

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("[" + furniName.ToUpper() + "] Error: " + ex);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[" + furniName.ToUpper() + "] Done!");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine();
            }






            Console.WriteLine("Finished! Press any key to exit...");

            if (!close_if_done)
            {
                Console.ReadKey();
            }
        }

        public static void rewriteFile(string furni, string dir, string file, string content)
        {
            if (File.Exists(@".\furniture\" + furni + @"-0\" + dir + @"\" + file + ".asasm"))
            {
                File.Delete(@".\furniture\" + furni + @"-0\" + dir + @"\" + file + ".asasm");
            }

            using (StreamWriter sw = new StreamWriter(@".\furniture\" + furni + @"-0\" + dir + @"\" + file + ".asasm"))
            {
                sw.Write(content);
            }
        }

    }
}
