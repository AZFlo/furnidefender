Before you do anything please configure the batch files (for example: RemoveDefenderAndSetAuthor.bat)
There are some options to setup. Change the RemoveDefenderAndSetAuthor.bat or similar files to your liking.
You need to change the /author=YourName

And if you don't want the message to appear on your website, change the /ignoreddomains= part.
Please spearate the domains by semicolon like: habbo.com;habbo.de;habbo.fr

So you get something like: /ignoreddomains=habbo.com;habbo.de;habbo.fr



1. Put your furniture flash files into the /furniture directory
2. Start FurniDefender.exe or one of the batch files for example: RemoveDefender.bat
3. Wait until you see "Finished! Press any key to exit..."
4. The result will be inside the /finished directory
4.1 If the process finishes, the application will exit if the /closeifdone parameter is one of your start arguments



The SetAuthor option is only an alternative "protection" for the furni - it'll only add a fancy console log to inform where the file is from:

"Ohh excuse me... but... the furni %furni_name% was made by me! Be a nice guy and give proper credits c: Kind regards, %author%


In my opinion that's the more suitable solution.

Kind regards
- CrEcEp